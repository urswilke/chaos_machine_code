## Introduction

The temperature measurements are done by DS18B20 thermometers, read in by a Adafruit Feather M0 Express micro-controller (but should work with any micro-controller that's capable to run arduino and has a usb port and a digital pin to connect the thermometers)
which send the data to python. The python code translates the temperatures into midi notes and sends these notes to fluidsynth which finally synthesizes them into sound in real time. The measurements generate sound with fluctuations of chaotic nature.


## Circuit

A description how the thermometers are wired up with the micro-controller can be for example found
[here](https://www.engineersgarage.com/esp8266/ds18b20-nodemcu-temperature-monitoring/) and
[here](https://lastminuteengineers.com/multiple-ds18b20-arduino-tutorial/).

## Install and setup arduino IDE on ubuntu

### Installation:

(explained [here](https://snapcraft.io/arduino))

```{bash}
sudo snap install arduino
```

### Add adafruit support

In order to set the micro-controller up for arduino see
[here](https://learn.adafruit.com/adafruit-feather-m0-basic-proto/setup) and
[here](https://learn.adafruit.com/adafruit-feather-m0-basic-proto/using-with-arduino-ide).

### Temperature measurements

The needed libraries have to be installed via the ide:
A tutorial to install the libraries [OneWire](https://github.com/PaulStoffregen/OneWire) &
[Dallas](https://github.com/milesburton/Arduino-Temperature-Control-Library) to cope with the thermometers can be found
[here](https://learn.adafruit.com/adafruit-all-about-arduino-libraries-install-use/) and
[here](https://learn.adafruit.com/adafruit-all-about-arduino-libraries-install-use/library-manager).

## Arduino code

### Temperature measurements

Similar projects are described in [this report](https://create.arduino.cc/projecthub/TheGadgetBoy/ds18b20-digital-temperature-sensor-and-arduino-9cc806)
and for multiple thermometers [this](https://lastminuteengineers.com/multiple-ds18b20-arduino-tutorial/) report.

You might need to choose the port: (s.th. like `/dev/ttyACM0` on my machine)
Arduino IDE -> tools -> port


### Upload to micro-controller

When everything is set up, the arduino file (see file in the arduino subfolder
of this repo) has to be uploaded to the micro-controller. When the
micro-controller is then reset it will run the uploaded code. This code makes
temperature measurements of all connected thermometers and prints them to the
serial connection.


## Python

### Serial connection

An example to read arduino serial from python can be found
[here](https://problemsolvingwithpython.com/11-Python-and-External-Hardware/11.04-Reading-a-Sensor-with-Python/).

### Install python libraries

In order to read in the serial connection python is used. Jupyter and some
libraries are needed. It's probably the easiest way to [install
anaconda](https://docs.anaconda.com/anaconda/install/), then [activate its conda
environment](https://docs.conda.io/projects/conda/en/latest/user-guide/getting-started.html#managing-environments),
and in this environment [install
mido and python-rtmidi](https://mido.readthedocs.io/en/latest/installing.html), as well as [music21](https://web.mit.edu/music21/doc/installing/index.html) (which are not included in anaconda), by running these commands:

```{bash}
pip install mido
pip install python-rtmidi
pip install music21
pip install miditapyr
pip install pyserial
```



### Run jupyter notebook

First run jupyter by typing

```{bash}
jupyter lab
```


in the command line. In Jupyter lab open the notebook file in the python
directory in this repo.  QSynth has to be started and the arduino sending to the
serial connection has to be connected to the computer via usb.

If everything is set up correctly, the python notebook can be run to produce sound. 🎊🎊🎊


## Install R libraries

The R code in this project is not needed to run the machine. It is rather supplementary.

First [R](https://cran.r-project.org/) and [RStudio](https://rstudio.com/products/rstudio/download/) need to be installed on your machine.

You can install the required R packages, by running the [installation script](R/package_install_script/package_install_script.R) on your machine. 