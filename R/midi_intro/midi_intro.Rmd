---
title: "From temperatures to midi format"
author: "Urs Wilke"
date: "11/23/2020"
output: github_document
---

```{r setup, include=FALSE}
# you need to install my small package on https://github.com/urswilke/pyramidi, by running:
# remotes::install_github("UrsWilke/pyramidi")

knitr::opts_chunk$set(echo = FALSE)
library(tidyverse)
library(reticulate)
reticulate::use_python("~/anaconda3/bin/python", required = T)
reticulate::source_python("../../python/functions.py")
source(here::here("utils.R"))
tb <- theme_blue()

```

```{python load_libs}
import music21
import pandas as pd
import numpy as np
```

## Midi and scales

### Very short midi intro

[Midi](https://en.wikipedia.org/wiki/MIDI) is a communications protocol for electronic 
musical instruments. In the midi format musical notes are represented by
integers from 0 to 127. A difference of 1 between two midi notes corresponds to
an interval of one semitone between the two notes. 12 semitones correspond to
one octave. The middle C note is at `midi = 60`.

### Scales in midi

A musical [scale](https://en.wikipedia.org/wiki/Scale_(music)) can be defined by
a set of `intervals` by just repeating these intervals in every octave (adding
multiples of 12 to the intervals). 

#### Chromatic scale 

The chromatic scale is simply given by all note intervals in an octave `[`r 0:11`]`. 
By repeating this through all octaves, this results in every `midi` note from 0 to 127:

```{python chromatic}
list(range(128))
```


#### C major scale

The C major scale is given by these intervals
from the root note (these are the distances upward from the red lines in the graph below):

```{python def_cmaj_intervals, echo=TRUE}
cmaj_intervals = [0, 2, 4, 5, 7, 9, 11]
```

The whole scale results in the following sequence of `midi` notes:

```{python scale_cmaj_intervals}
cmaj_scale = get_chord_scale_notes(cmaj_intervals)
cmaj_scale
```

<!-- You can also get the `midi` notes of the C major scale using the [music21](http://web.mit.edu/music21/) package: -->

<!-- ```{python c_scale, echo=TRUE} -->
<!-- scale_obj = music21.scale.MajorScale('c') -->
<!-- [p.midi for p in scale_obj.getPitches('a0', 'g8')] -->

<!-- ``` -->

#### C minor pentatonic scale

The note intervals of the minor pentatonic scale from the root note are given by

```{python def_intervals, echo=TRUE}
cminp_intervals = [0, 3, 5, 7, 10]
```

resulting in these scale `midi` notes:

```{python scale_intervals}
cminp_scale = get_chord_scale_notes(cminp_intervals)
cminp_scale
```

From every multiple of 12 the same 5 intervals are repeated.


You can find examples how to technically use different scales at the url of the
[chaosmachine](https://gitlab.com/urswilke/chaosmachine#generating-multiple-midifiles)
package.



## Mapping temperature differences `r "\u0394"` to midi notes

The chaos machine translates temperature differences `r "\u0394"`
[linearly](https://en.wikipedia.org/wiki/Linear_equation) (characterized by a
slope and a y-intercept) to midi notes and then rounding to the closest integer
midi notes in the scale. Rounding the signal to the notes of the scale can be 
seen as some sort of
[autotuning](https://en.wikipedia.org/wiki/Pitch_correction).

#### C major scale

Let's have a look at the   <span style="color: blue;">`r "\u0394"` to midi translation curve</span> for the C major scale choosing a slope of 1 semitone / 1 °C and the middle C note (midi = 60) as the y-intercept.


```{r make_df}
yintercept <- 60
slope <- 1
Delta <- seq(-20, 16, .1)
scale <- py$cmaj_scale
make_T_to_midi_table <- function(Delta, slope, scale, yintercept) {
  midi <- 
    Delta %>% 
    map_dbl(~get_closest_int(.x * slope, scale, yintercept))
  
  df <- 
    tibble(Delta, midi) %>% 
    left_join(pyramidi::midi_defs, by = c("midi" = "note"))
  df  
}
df <- make_T_to_midi_table(Delta, slope, scale, yintercept)
dfc <- make_T_to_midi_table(Delta, slope, py$cminp_scale, yintercept)
# write_csv(df, "df.csv")
```



```{r plot_curve, warning=FALSE}
plot_T_to_midi <- function(df) {
  dfn <- df %>% select(-Delta) %>% distinct()
  tick_labels <- dfn$note_name %>% sort() %>% as.character()
  breaks <- dfn$midi %>% unique() %>% sort()

  df %>% 
    ggplot(aes(x = Delta, y = midi)) +
    geom_hline(
      data = df %>% select(midi) %>% distinct(),
      aes(yintercept = midi),
      size = 0.3,
      linetype = "dotted",
      color = "blue"
    ) +
    geom_hline(
      data = tibble(midi = c(48, 60, 72)),
      aes(yintercept = midi),
      color = "red"
    ) +
    geom_step(color = "blue") +
    xlab("\u0394 (°C)") +
    theme_bw() + 
    geom_rect(
      data = pyramidi::piano_keys_coordinates %>% 
        left_join(pyramidi::midi_defs, by = c("midi" = "note")) %>% 
        arrange(layer) %>% 
        mutate(text = note_name,
               layer = as_factor(layer)), 
      aes(xmin = 22 - 5*ymin, 
          xmax = 22 - 5*ymax, 
          ymin = xmin, 
          ymax = xmax, 
          fill = layer,
          text = note_name), 
      inherit.aes = F,
      color = "black",
      show.legend = FALSE
    ) + 
    coord_cartesian(
      xlim = c(-20, 20),
      ylim = c(breaks[1], breaks[length(breaks)])
    ) + 
    scale_fill_manual(values = c("#ffffdd", "#113300")) +
    scale_y_continuous(
      breaks = breaks,
      minor_breaks = breaks[1]:breaks[length(breaks)],
      labels = tick_labels,
      "note",
      sec.axis = sec_axis( ~ . , name = "midi")
    ) +
    ggtitle("Assignment of temperature differences \u0394 to midi notes")
}
p1 <- plot_T_to_midi(df) + labs(subtitle = "C major scale")
ggsave(p1 + tb, filename = "T_to_midi_Cmaj.svg",  bg = "transparent", width = 3.36 * 3, height = 2.4 * 3)
ggsave(
  p1 + tb, 
  filename = "/home/chief/R/urssblogg/content/post/2020-11-19-synthesizing-temperature-measurements-into-sound/T_to_midi_Cmaj.svg",  
  bg = "transparent", 
  width = 3.36 * 3, 
  height = 2.4 * 3
)
ggsave(p1 + tb +   
         theme(panel.background=element_blank(), 
               panel.grid.major = element_blank(),
               panel.grid.minor = element_blank(),
               axis.ticks = element_blank(),
               axis.text.x = element_blank(),
               axis.text.y = element_blank(),
               axis.title.x = element_blank(),
               axis.title.y = element_blank(),
               plot.background = element_rect(fill = "transparent", colour = NA),
               plot.margin = unit(c(0, 0, 0, 0), "cm"),  # Edited code
               legend.position = 'none') + 
         ggtitle(""), 
       filename = "/home/chief/R/urssblogg/content/post/2020-11-19-synthesizing-temperature-measurements-into-sound/featured.png",  
       bg = "transparent", 
       width = 3.36, 
       height = 2.4)
p1
```

The scale notes (indicated on the left of the graph) are shown as the thin
horizontal grid lines and correspond to the white keys fot the C major scale.
The <span style="color: red;">C root notes</span> of the scale are 12 midi notes
apart.


#### C minor pentatonic scale

Choosing the C minor pentatonic scale instead looks like this:

```{r c_minor_pent, warning=FALSE}
p2 <- plot_T_to_midi(dfc) + labs(subtitle = "C minor pentatonic")
ggsave(p2 + tb, filename = "T_to_midi_Cmin_pent.svg",  bg = "transparent", width = 3.36 * 3, height = 2.4 * 3)
p2
```


The chromatic scale step function would simply be a step function at every 
integer midi value.

### Slope and y-intercept

```{r mb, include=FALSE}
graph_slopes <-   c(.1, .5, 1, 2, 3)
graph_yintercepts <-   c(48, 55, 60, 67, 72)
```

For the varying slopes of `r glue::glue_collapse(graph_slopes, ", ", last = " and ")` and 
y-intercepts of `r glue::glue_collapse(graph_yintercepts, ", ", last = " and ")` the
resulting `r "\u0394"` to midi curves would be:


```{r slopes, include=FALSE}
dfs <- 
  graph_slopes %>% 
  set_names() %>% 
  map_dfr(~make_T_to_midi_table(Delta, .x, scale, yintercept), .id = "slope")
dfs
```


```{r yints, include=FALSE}
dfr <- 
  graph_yintercepts %>% 
  set_names() %>% 
  map_dfr(~make_T_to_midi_table(Delta, slope, scale, .x), .id = "y-intercept")
dfr
```


```{r plot}
p1 <- 
  dfs %>% 
  ggplot(aes(x = Delta, y = midi, color = slope)) +
  geom_step() +
  theme_bw()
p2 <- 
  dfr %>% 
  ggplot(aes(x = Delta, y = midi, color = `y-intercept`)) +
  geom_step() +
  theme_bw()
library(patchwork)
p1 / p2
```

