From temperatures to midi format
================
Urs Wilke
11/23/2020

## Midi and scales

### Very short midi intro

[Midi](https://en.wikipedia.org/wiki/MIDI) is a communications protocol
for electronic musical instruments. In the midi format musical notes are
represented by integers from 0 to 127. A difference of 1 between two
midi notes corresponds to an interval of one semitone between the two
notes. 12 semitones correspond to one octave. The middle C note is at
`midi = 60`.

### Scales in midi

A musical [scale](https://en.wikipedia.org/wiki/Scale_(music)) can be
defined by a set of `intervals` by just repeating these intervals in
every octave (adding multiples of 12 to the intervals).

#### Chromatic scale

The chromatic scale is simply given by all note intervals in an octave
`[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]`. By repeating this through all
octaves, this results in every `midi` note from 0 to 127:

    ## [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127]

#### C major scale

The C major scale is given by these intervals from the root note (these
are the distances upward from the red lines in the graph below):

``` python
cmaj_intervals = [0, 2, 4, 5, 7, 9, 11]
```

The whole scale results in the following sequence of `midi` notes:

    ## [0, 2, 4, 5, 7, 9, 11, 12, 14, 16, 17, 19, 21, 23, 24, 26, 28, 29, 31, 33, 35, 36, 38, 40, 41, 43, 45, 47, 48, 50, 52, 53, 55, 57, 59, 60, 62, 64, 65, 67, 69, 71, 72, 74, 76, 77, 79, 81, 83, 84, 86, 88, 89, 91, 93, 95, 96, 98, 100, 101, 103, 105, 107, 108, 110, 112, 113, 115, 117, 119, 120, 122, 124, 125, 127]

<!-- You can also get the `midi` notes of the C major scale using the [music21](http://web.mit.edu/music21/) package: -->
<!-- ```{python c_scale, echo=TRUE} -->
<!-- scale_obj = music21.scale.MajorScale('c') -->
<!-- [p.midi for p in scale_obj.getPitches('a0', 'g8')] -->
<!-- ``` -->

#### C minor pentatonic scale

The note intervals of the minor pentatonic scale from the root note are
given by

``` python
cminp_intervals = [0, 3, 5, 7, 10]
```

resulting in these scale `midi` notes:

    ## [0, 3, 5, 7, 10, 12, 15, 17, 19, 22, 24, 27, 29, 31, 34, 36, 39, 41, 43, 46, 48, 51, 53, 55, 58, 60, 63, 65, 67, 70, 72, 75, 77, 79, 82, 84, 87, 89, 91, 94, 96, 99, 101, 103, 106, 108, 111, 113, 115, 118, 120, 123, 125, 127]

From every multiple of 12 the same 5 intervals are repeated.

You can find examples how to technically use different scales at the url
of the
[chaosmachine](https://gitlab.com/urswilke/chaosmachine#generating-multiple-midifiles)
package.

## Mapping temperature differences Δ to midi notes

The chaos machine translates temperature differences Δ
[linearly](https://en.wikipedia.org/wiki/Linear_equation) (characterized
by a slope and a y-intercept) to midi notes and then rounding to the
closest integer midi notes in the scale. Rounding the signal to the
notes of the scale can be seen as some sort of
[autotuning](https://en.wikipedia.org/wiki/Pitch_correction).

#### C major scale

Let’s have a look at the <span style="color: blue;">Δ to midi
translation curve</span> for the C major scale choosing a slope of 1
semitone / 1 °C and the middle C note (midi = 60) as the y-intercept.

![](midi_intro_files/figure-gfm/plot_curve-1.png)<!-- -->

The scale notes (indicated on the left of the graph) are shown as the
thin horizontal grid lines and correspond to the white keys fot the C
major scale. The <span style="color: red;">C root notes</span> of the
scale are 12 midi notes apart.

#### C minor pentatonic scale

Choosing the C minor pentatonic scale instead looks like this:

![](midi_intro_files/figure-gfm/c_minor_pent-1.png)<!-- -->

The chromatic scale step function would simply be a step function at
every integer midi value.

### Slope and y-intercept

For the varying slopes of 0.1, 0.5, 1, 2 and 3 and y-intercepts of 48,
55, 60, 67 and 72 the resulting Δ to midi curves would be:

![](midi_intro_files/figure-gfm/plot-1.png)<!-- -->
