Read and visualize midi files
================

## Introduction

In this document, a piano roll visualization is produced of the midifile
written at the end of the python notebook. And I will promote the two
packages I wrote:

-   The python package
    [miditapyr](https://github.com/urswilke/miditapyr/) can tabulate
    midifile data (read in by [mido](github.com/mido/mido)) in
    dataframes.
-   The R package [pyramidi](https://github.com/urswilke/pyramidi/) can
    transform these dataframes to a wide format that allows for an easy
    way to plot piano roll plots.

``` r
library(tidyverse)
library(pyramidi)
mt <- reticulate::import("miditapyr")
mido <- reticulate::import("mido")
```

## Read in the midi file as a mido file object

``` r
midifile <- here::here("..", "python", "recorded_data", "live_record.mid")
mf <- mido$MidiFile(midifile)
mf
```

    ## <midi file '/home/chief/chaos_machine/arduino/my_code/R/../python/recorded_data/live_record.mid' type 1, 10 tracks, 132 messages>

## Read in the midi file to a dataframe

``` r
md <- mt$midi_to_df(mf) 
head(md, 20)
```

    ##    i_track  meta                        msg
    ## 1        0 FALSE      note_on, 0, 48, 64, 0
    ## 2        0 FALSE note_off, 10668, 48, 64, 0
    ## 3        0 FALSE      note_on, 0, 47, 64, 0
    ## 4        0 FALSE   note_off, 937, 47, 64, 0
    ## 5        0 FALSE      note_on, 0, 48, 64, 0
    ## 6        0 FALSE note_off, 14968, 48, 64, 0
    ## 7        0  TRUE            end_of_track, 0
    ## 8        1 FALSE      note_on, 0, 47, 64, 1
    ## 9        1 FALSE  note_off, 9733, 47, 64, 1
    ## 10       1 FALSE      note_on, 0, 48, 64, 1
    ## 11       1 FALSE  note_off, 6551, 48, 64, 1
    ## 12       1 FALSE      note_on, 0, 50, 64, 1
    ## 13       1 FALSE note_off, 10289, 50, 64, 1
    ## 14       1  TRUE            end_of_track, 0
    ## 15       2 FALSE      note_on, 0, 57, 64, 2
    ## 16       2 FALSE  note_off, 2249, 57, 64, 2
    ## 17       2 FALSE      note_on, 0, 59, 64, 2
    ## 18       2 FALSE  note_off, 2804, 59, 64, 2
    ## 19       2 FALSE      note_on, 0, 60, 64, 2
    ## 20       2 FALSE  note_off, 2809, 60, 64, 2

The list column `msg` consists of named vectors. However, their names
are not printed. To know what these quantities stand for, have a look at
the column names of the following dataframe output.

## Put midi data frame in a tidy format

``` r
mdt <- mt$tidy_df(md) %>% as_tibble()
mdt 
```

    ## # A tibble: 132 × 7
    ##    i_track meta  type          time  note velocity channel
    ##      <dbl> <lgl> <chr>        <dbl> <dbl>    <dbl>   <dbl>
    ##  1       0 FALSE note_on          0    48       64       0
    ##  2       0 FALSE note_off     10668    48       64       0
    ##  3       0 FALSE note_on          0    47       64       0
    ##  4       0 FALSE note_off       937    47       64       0
    ##  5       0 FALSE note_on          0    48       64       0
    ##  6       0 FALSE note_off     14968    48       64       0
    ##  7       0 TRUE  end_of_track     0   NaN      NaN     NaN
    ##  8       1 FALSE note_on          0    47       64       1
    ##  9       1 FALSE note_off      9733    47       64       1
    ## 10       1 FALSE note_on          0    48       64       1
    ## # … with 122 more rows

## Add cumulative time `t`

-   In the midi file format time is [measured in
    ticks](https://mido.readthedocs.io/en/latest/midi_files.html#tempo-and-beat-resolution)
    as delta time passed since the last event. This adds the cumulative
    time `t` passed.
-   *This also adds time measured in musical measures (bars) `m` and
    beats (quarter notes) `b`, which doesn’t make much sense in this
    example, as the time increments are given by arduino code on the
    micro-controller and the python loop. So don’t try to find a deeper
    meaning in these quantities here.*

``` r
pyramidi:::tab_time_sig(mdt)
```

    ## # A tibble: 1 × 4
    ##   numerator denominator clocks_per_click notated_32nd_notes_per_beat
    ##       <dbl>       <dbl>            <dbl>                       <dbl>
    ## 1         4           4               24                           8

``` r
mdm <- tab_measures(mdt, mf$ticks_per_beat)

mdm
```

    ## # A tibble: 132 × 12
    ##    i_track meta  type        time  note velocity channel ticks     t     m     b
    ##      <dbl> <lgl> <chr>      <dbl> <dbl>    <dbl>   <dbl> <dbl> <dbl> <dbl> <dbl>
    ##  1       0 FALSE note_on        0    48       64       0     0   0     0     0  
    ##  2       0 FALSE note_off   10668    48       64       0 10668  11.1  22.2  88.9
    ##  3       0 FALSE note_on        0    47       64       0 10668  11.1  22.2  88.9
    ##  4       0 FALSE note_off     937    47       64       0 11605  12.1  24.2  96.7
    ##  5       0 FALSE note_on        0    48       64       0 11605  12.1  24.2  96.7
    ##  6       0 FALSE note_off   14968    48       64       0 26573  27.7  55.4 221. 
    ##  7       0 TRUE  end_of_tr…     0   NaN      NaN     NaN 26573  27.7  55.4 221. 
    ##  8       1 FALSE note_on        0    47       64       1     0   0     0     0  
    ##  9       1 FALSE note_off    9733    47       64       1  9733  10.1  20.3  81.1
    ## 10       1 FALSE note_on        0    48       64       1  9733  10.1  20.3  81.1
    ## # … with 122 more rows, and 1 more variable: i_note <int>

## Pivot dataframe to wide

This changes the data format in a way that note\_on and note\_off events
of the same notes occur in the same line.

``` r
mdw <- widen_events(mdm)
mdw
```

    ## # A tibble: 71 × 23
    ##    i_track meta   note channel i_note time_note_on time_note_off
    ##      <dbl> <lgl> <dbl>   <dbl>  <int>        <dbl>         <dbl>
    ##  1       0 FALSE    48       0      1            0         10668
    ##  2       0 FALSE    47       0      1            0           937
    ##  3       0 FALSE    48       0      2            0         14968
    ##  4       0 TRUE    NaN     NaN      0           NA            NA
    ##  5       1 FALSE    47       1      1            0          9733
    ##  6       1 FALSE    48       1      1            0          6551
    ##  7       1 FALSE    50       1      1            0         10289
    ##  8       1 TRUE    NaN     NaN      0           NA            NA
    ##  9       2 FALSE    57       2      1            0          2249
    ## 10       2 FALSE    59       2      1            0          2804
    ## # … with 61 more rows, and 16 more variables: time_end_of_track <dbl>,
    ## #   velocity_note_on <dbl>, velocity_note_off <dbl>,
    ## #   velocity_end_of_track <dbl>, ticks_note_on <dbl>, ticks_note_off <dbl>,
    ## #   ticks_end_of_track <dbl>, t_note_on <dbl>, t_note_off <dbl>,
    ## #   t_end_of_track <dbl>, m_note_on <dbl>, m_note_off <dbl>,
    ## #   m_end_of_track <dbl>, b_note_on <dbl>, b_note_off <dbl>,
    ## #   b_end_of_track <dbl>

``` r
source(here::here("utils.R"))
t <- theme_blue()
```

    ## Warning: `panel.margin` is deprecated. Please use `panel.spacing` property
    ## instead

## Visualize the midi data

``` r
# Add musical note names 

# The midi notes are integers between 0 and 127. The usual names of the notes
# (as well as the octave they're in) is in this table ...:
pyramidi::midi_defs
```

    ## # A tibble: 128 × 2
    ##     note note_name
    ##    <int> <fct>    
    ##  1     0 C-1      
    ##  2     1 C#-1     
    ##  3     2 D-1      
    ##  4     3 Eb-1     
    ##  5     4 E-1      
    ##  6     5 F-1      
    ##  7     6 F#-1     
    ##  8     7 G-1      
    ##  9     8 G#-1     
    ## 10     9 A-1      
    ## # … with 118 more rows

``` r
p <- mdw %>%
  # which will be added to the dataframe:
  left_join(pyramidi::midi_defs, by = "note") %>% 
  # end_of_track events are automatically added when mido saves a file.
  # Furthermore, the loop in the main python notebook ends without sending
  # note_off events (see mdw output) -> we will omit these events that don't
  # make sense in this context:
  drop_na(time_note_off) %>% 
  ggplot(aes(
      x = t_note_on,
      y = note_name,
      xend = t_note_off,
      yend = note_name,
      color = as_factor(channel)
    ), alpha = 0.5) +
  geom_segment() +
  geom_point() +
  geom_point(aes(t_note_off, note_name), shape = 3) +
  labs(
    title = "Piano roll of the note events in the midi file",
    subtitle = "(notes not played omitted on the y-axis)"
  ) +
  xlab("Time (s)") +
  ylab("Note") +
  scale_colour_discrete(name = "Channel") +
  theme_minimal() +
  t
p
```

![](plot_midifile_files/figure-gfm/midi_piano_roll-1.png)<!-- -->

    ## Saving 7 x 5 in image
    ## Saving 7 x 5 in image
