Visualization of the data values in the loop
================
Urs Wilke
11/20/2020

``` r
library(tidyverse)
library(patchwork)
```

## Report

plot the data in the csv files written in python/T\_to\_midi.ipynb

``` r
# Read data:
df_temp <- readr::read_csv(here::here("..", "python", "recorded_data", "/temp_data_recording.csv"))
df_diff <- readr::read_csv(here::here("..", "python", "recorded_data", "/diff_data_recording.csv"))
df_mid <- readr::read_csv(here::here("..", "python", "recorded_data", "midi_data_recording.csv"))

# prepare plots:
df_temp_plot <- df_temp %>%
  mutate_at(vars(-c("time")),
            ~str_remove(., ".*'T': '") %>%
              str_remove("'.*")) %>%
  mutate_if(is_character, as.numeric) %>%
  gather(S, T, -time)
p1 <- df_temp_plot %>%
  ggplot(aes(time, T, color = S, shape = S)) +
  geom_line() +
  geom_point() + 
  theme(
    axis.text.x = element_blank()
  ) + xlab("")


df_diff_plot <-
  df_diff %>%
  mutate_at(vars(-c("time")),
            ~str_remove(., ".*'T': ") %>%
              str_remove("\\}")) %>%
  gather(pair, diff, -time) %>% 
  mutate(diff = as.numeric(diff))

p2 <- df_diff_plot %>%
  ggplot(aes(time, diff, color = pair)) +
  geom_line() +
  geom_point() + 
  theme(
    axis.text.x = element_blank()
  ) + xlab("") + ylab("\u0394")

df_mid_plot <-
  df_mid %>%
  mutate_at(vars(-c("time")),
            ~str_remove(., ".*'note': ") %>%
              str_remove(",.*")) %>%
  gather(pair, midi, -time) %>% 
  mutate(midi = as.numeric(midi))

p3 <- df_mid_plot %>%
  ggplot(aes(time, midi, color = pair)) +
  geom_line() +
  geom_point()
```

``` r
source(here::here("utils.R"))
t <- theme_blue()
```

    ## Warning: `panel.margin` is deprecated. Please use `panel.spacing` property
    ## instead

``` r
# patch them together:
patch <- (p1 + t) / (p2 + t) / (p3 + t)  + 
  plot_layout(guides = "collect") + 
  plot_annotation(
    title = 'Temperatures, their differences \u0394 & resulting midi notes',
    subtitle = 'At \u0394 = 0 the midi notes are mapped on different notes',
    theme = t) 
patch
```

![](plot_csv_data_files/figure-gfm/csv_vis-1.png)<!-- -->

    ## Saving 7 x 5 in image
    ## Saving 7 x 5 in image
