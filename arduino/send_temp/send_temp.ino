// arduino sketch to read multiple DS18B20 thermometers and then print the information to the serial connection

// original code from:
//https://arduino.stackexchange.com/questions/18919/how-to-read-temperature-from-multiple-ds18b20-faster
#include <OneWire.h>
#include <DallasTemperature.h>

#define ONE_WIRE_BUS 5
#define TEMPERATURE_PRECISION 11
#define BAUD_RATE 9600

// Setup a oneWire instance to communicate with any OneWire devices (not just Maxim/Dallas temperature ICs)
OneWire oneWire(ONE_WIRE_BUS);

// Pass our oneWire reference to Dallas Temperature. 
DallasTemperature sensors(&oneWire);

// arrays to hold device addresses
DeviceAddress devices[20];
int deviceCount = 0;

void setup(void)
{
  // start serial port
  Serial.begin(BAUD_RATE);
  //Serial.println("Dallas Temperature IC Control Library Demo");

  // Start up the library
  sensors.begin();
  delay(1000);

  // locate devices on the bus
  Serial.print("Locating devices...");
  delay(1000);
  Serial.print("Found ");
  delay(1000);
  Serial.print(sensors.getDeviceCount(), DEC);
  delay(1000);
  Serial.println(" devices.");
  delay(1000);

  deviceCount = sensors.getDeviceCount();  
  delay(1000);


  // report parasite power requirements
  Serial.print("Parasite power is: "); 
  delay(1000);
  if (sensors.isParasitePowerMode()) 
  {
    Serial.println("ON");
  }
  else 
  {
    Serial.println("OFF");
  }
  delay(1000);

  for (int i = 0; i < deviceCount; i++)
  {
    if (!sensors.getAddress(devices[i], i)) 
    {
      Serial.println("Unable to find address for Device" + i); 
      delay(100);
    }
  }

  // show the addresses we found on the bus
  for (int i = 0; i < deviceCount; i++)
  {    
    Serial.print("Device " + (String)i + " Address: ");
    delay(100);
    printAddress(devices[i]);
    delay(100);
    Serial.println();
    delay(1000);
  }

  for (int i = 0; i < deviceCount; i++)
    sensors.setResolution(devices[i], TEMPERATURE_PRECISION);

  delay(1000);
  
}

// function to print a device address
void printAddress(DeviceAddress deviceAddress)
{
  for (uint8_t i = 0; i < 8; i++)
  {
    // zero pad the address if necessary
    if (deviceAddress[i] < 16) Serial.print("0");
    Serial.print(deviceAddress[i], HEX);
    delay(100);
  }


}

// function to print a device address
String returnAddress(DeviceAddress deviceAddress)
{
  String address_string = "";
  for (uint8_t i = 0; i < 8; i++)
  {
    address_string += deviceAddress[i];
//    // zero pad the address if necessary
//    if (deviceAddress[i] < 16) Serial.print("0");
//    Serial.print(deviceAddress[i], HEX);
//    delay(100);
  }
  return address_string;


}

// function to print the temperature for a device
String printTemperature(DeviceAddress deviceAddress)
{
  float tempC = sensors.getTempC(deviceAddress);
  return (String)tempC;
}

// function to print a device's resolution
void printResolution(DeviceAddress deviceAddress)
{
  Serial.print("Resolution: ");
  Serial.print(sensors.getResolution(deviceAddress));
  Serial.println();    
//  delay(100);
}

// main function to print information about a device
void printData(DeviceAddress deviceAddress)
{
  Serial.print("Device Address: ");
  printAddress(deviceAddress);
  Serial.print(" ");
  printTemperature(deviceAddress);
  Serial.println();
//  delay(100);
}

void loop(void)
{ 
  if (deviceCount == 0)
  {
     Serial.println("No devices found.");
     return;
  }
  String id_array[deviceCount];
  for (int i = 0; i < deviceCount; i++)
  {    
    id_array[i] = returnAddress(devices[i]);
    delay(100);
  }

  sensors.requestTemperatures();

  String string_to_send = "";

  // print the device information
  for (int i = 0; i < deviceCount; i++)
  {
    string_to_send += "ID: ";
    string_to_send += id_array[i];
    string_to_send += ", T: ";
    string_to_send += printTemperature(devices[i]);
    if (i != deviceCount - 1)
      string_to_send += "; ";    
    delay(10);  
  }

  Serial.println(string_to_send);
//  delay(100);
}
