# functions that are imported in the T_to_midi notebook
import itertools
import numpy as np
from mido import Message, MidiFile, MidiTrack
import pandas as pd

def split_serial(serial_string):
    temp_dicts = []
    string_n = serial_string.decode()  # decode byte string into Unicode
    serial_string = string_n.rstrip() # remove \n and \r

    # Run over all thermometers:
    thermo_parts = serial_string.split('; ')
    for thermo_part in thermo_parts:
        # create empty dictionary:
        temp_dict = dict()
        # split the sensor number and ID part from the temperature part:
        # (separated by ","):
        str_parts = thermo_part.split(', ')
        for s_part in str_parts:
            # split the part containing the key ("ID" & "T" for "temperature")
            # from the part containing the value (after the ": "):
            s_parts = s_part.split(': ')
            d_part = dict([s_parts])
            # add the data to the dictionary:
            temp_dict.update(d_part)
        temp_dicts.append(temp_dict)
    return temp_dicts


def get_note_on_dict(diff_dicts, scales, root_at_0K, slopes, channels, min_note = None, max_note = None):
    note_on_dicts = []
    # print(len(diff_dicts))
    for i in range(len(diff_dicts)):
        thermo_dict = diff_dicts[i]
        note = get_closest_int(float(slopes[i] * thermo_dict['T']), scales[i], root_at_0K[i])
        if min_note is not None:
            note = max(min_note, note)
        if max_note is not None:
            note = min(max_note, note)
        # print(note_dict)
        # add 'type': 'note_on' to the dictionary:
        msg_dict = {'type': 'note_on', 'note': note, 'channel': channels[i]}
        note_on_dicts.append(msg_dict)

    return note_on_dicts

def get_note_off_dict(note_on_dicts):
    note_off_dicts = []
    for note_on_dict in note_on_dicts:
        # replace 'type': by 'note_off' in the dictionary:
        note_off_dict = {**note_on_dict, **{'type': 'note_off'}}
        note_off_dicts.append(note_off_dict)
    return note_off_dicts



def get_closest_int(floatval, int_list, root_at_0K):
    # from here: https://stackoverflow.com/questions/12141150/from-list-of-integers-get-number-closest-to-a-given-value
    a = min(int_list, key=lambda x:abs(x - (floatval + root_at_0K)))
    return a


def derive_diffs(temp_dicts):
    # https://stackoverflow.com/questions/40092474/what-is-the-fastest-way-to-get-all-pairwise-combinations-in-an-array-in-python/40092582
    tuple_list = list(itertools.combinations(temp_dicts, 2))
    # take absolute value of temperature difference:
    diff_list = [abs(round(float(i[0]['T']) - float(i[1]['T']), 4)) for i in tuple_list]
    # https://stackoverflow.com/questions/54849211/create-list-of-dictionaries-from-two-lists-using-for-loop-in-python
    diff_dicts = []
    for j in diff_list:
        diff_dicts.append({"T": j})

    return diff_dicts


def get_chord_scale_notes(chord):
    octaves = np.array([12 * (j // len(chord)) for j in range(len(chord) * 11)])

    scale_pitch = np.array(chord * 11)

    chord_midi_notes = octaves + scale_pitch
    chord_midi_notes = chord_midi_notes[chord_midi_notes <= 127]
    chord_midi_notes = list(chord_midi_notes)
    return chord_midi_notes


def midi_port_to_file(msg_times, msg_data, filename):
    df = pd.DataFrame({'time': msg_times, 'data': msg_data}).explode('data').dropna()
    # exploding creates repeated indices for events occurring at the same time.
    # -> create ascending indices:
    df.reset_index(inplace=True)
    
    # create data frame of events' data:
    l2 = []
    for idx in range(len(df)):
        l2.append({**vars(df.data[idx])})
    df_events = pd.DataFrame(l2).drop('time', axis=1)
    df = pd.concat([df, df_events], axis=1)

    # https://stackoverflow.com/questions/22923775/calculate-pandas-dataframe-time-difference-between-two-columns-in-hours-and-minu
    df['time_diff'] = (pd.to_datetime(df['time']) - pd.to_datetime(df.groupby(['channel'])['time'].shift(1))).astype('timedelta64[ms]').fillna(0)

    # first element is nan ...:
    # df.loc[0, 'time_diff'] = 0
    df.time_diff = df.time_diff.astype('int')
    # replace time values in event data by the time increments recorded:
    l = []
    for idx in range(len(df)):
        l.append({**vars(df.data[idx]), **{'time': df.time_diff[idx]}})
    df['midi'] = l

    # create midi file:
    tracks = df_events.channel.unique()

    mid = MidiFile()
    l_track = []
    for i in tracks:
        l_track.append(MidiTrack())
        mid.tracks.append(l_track[i])
    for idx in range(len(df)):
        i_track = df_events['channel'][idx]
        l_track[i_track].append(Message(**df.midi[idx]))
    mid.save(filename)
