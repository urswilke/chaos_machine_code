
<!-- README.md is generated from README.Rmd. Please edit that file -->

# Python code directory

In order to run the chaos machine, follow the instructions on the
[chaosmachine package
website](https://gitlab.com/urswilke/chaosmachine).

What the package does corresponds to the steps taken in
[T\_to\_midi.ipynb](T_to_midi.ipynb) (part of the code is outsourced to
[functions.py](functions.py)). Alternatively to the package you can run
this notebook to:

  - read in the temperature data sent via a serial connection by the
    [micro-controller](../arduino/send_temp/send_temp.ino)
  - calculate all pairwise temperature differences
  - translate these differences to midi notes according to the chosen
    musical scales
  - send these midi notes to fluidsynth in real time
  - write this data to [csv files](python/recorded_data)
  - generate a [midi file](python/recorded_data/live_record.mid) and
    synthesize it to [mp3](python/recorded_data/live_record.mp3) using
    an [sf2 soundfont file](https://musical-artifacts.com/artifacts/387)

The notebook
[play\_soundfont.ipynb](play_soundfont/play_soundfont.ipynb) is a
minimal example sending a sequence of ascending midi notes to
fluidsynth. Afterwards, the notes are saved to a midi file which is then
rendered to an audio file.
