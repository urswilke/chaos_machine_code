---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.2'
      jupytext_version: 1.6.0
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

## Translate temperature measurements to midi notes to synthesize with fluidsynth


python real-time loop to

* read serial string, extract temperatures
* calculate temperature differences
* translate differences to midi notes according to a specified musical scale
* send the note events to a midi port in real time 

During the loop the data is recorded in lists and when it has finished the data is written to a midi and csv files. 


```python
import serial
import time
import datetime
import mido
import music21
import pandas as pd
import numpy as np
import random
import glob
import subprocess
import pathlib
```

Load functions in the file `functions.py` that has to be stored in the same directory as this notebook:

```python
%run functions.py
```

### Setup fluidsynth connection


Start Qsynth.

Then get the names of all output ports: 

```python
all_outports = mido.get_output_names()
all_outports
```

The port containing fluidsynth (which is started by QSynth) is chosen:

```python
fluidport = [s for s in all_outports if "FLUID Synth" in s]
fluidport
```

open a midi port and connect to fluidsynth:

```python
outport = mido.open_output(fluidport[0])
```

### Midi notes in scale


Choose by chord...:

```python
chord = [0, 3, 5, 7, 10]

scale_midi_notes = get_chord_scale_notes(chord)
# to get their names:
scale_notes =  [str(music21.note.Note(i).pitch) for i in scale_midi_notes]
# Show the first 10 notes in scale:
scale_notes[:10], scale_midi_notes[:10]
```

... alternatively choose by music21 scale object:

```python
scale_obj = music21.scale.MinorScale('a')
scale_notes = [str(p) for p in scale_obj.getPitches('a0', 'g8')]
scale_midi_notes = [p.midi for p in scale_obj.getPitches('a0', 'g8')]
# Show the first 10 notes in scale:
scale_notes[:10],  scale_midi_notes[:10]
```

...or choose chromatic scale:

```python
# chromatic scale:
# scale_midi_notes = [i for i in range(128)]
```

## Arduino 


Setup connection to Arduino serial line:

```python
dev_port = glob.glob('/dev/ttyACM*')[0]
dev_port
```

```python
# on my ubuntu machine the serial port is located at '/dev/ttyACM1' OR '/dev/ttyACM0'
# ser = serial.Serial('/dev/ttyACM0', 9600)
serial_port = serial.Serial(dev_port, 9600)

time.sleep(0.5)

```

Read one line of the serial connection

```python
serial_string = serial_port.readline()
serial_string
```

create a data directory if it doesn't exist:

```python
pathlib.Path("recorded_data").mkdir(parents=True, exist_ok=True)
```

and save the serial string to a file:

```python
with open("recorded_data/serial_string.txt", "w") as text_file:
    text_file.write("The serial string sent from the arduino is of the following format:\n")
    text_file.write(serial_string.decode())


```

print out the thermometer ids:

```python
serial_string
```

```python
id_list = [i['ID'] for i in split_serial(serial_string)]
id_list
```

## Loop parameters


Number of thermometers found:

```python
n_thermos = len(id_list)
n_thermos
```

Number of possible combinations of all thermometers:

```python
n_diffs = int((n_thermos - 1) * n_thermos / 2)
n_diffs
```

```python
# The scale and the root midi note that's played at 0 K can be chosen of every pair of thermometers:
# The following pairs of thermometers exist:
pairs = list(itertools.combinations([i+1 for i in range(n_thermos)], 2))
pairs
```

```python
# Choose scales for the n_diffs combinations:
scales = [scale_midi_notes] * n_diffs
```

```python
# choose root notes:
# set all root midi notes to 60: 
root_at_0K = [48] * n_diffs
# or choose individually:
root_at_0K = [48, 48, 55, 55, 53, 53, 72, 72, 60, 60]
root_at_0K
```

Slopes of the linear mapping of temperature differences $T_2 - T_1$ to semi-tones $mid_2 - mid_1$:
$$\frac{mid_2 - mid_1}{T_2 - T_1}$$

```python
slopes = [2] * n_diffs
slopes
```

Midi channels:

```python
channels = list(range(n_diffs))
channels
```

## Loop


* reads arduino serial connection, 
* translates the measurements to midi notes, 
* sends the midi notes to the QSynth port, and 
* records all the data in lists

```python
n_total_timesteps = 10


# empty lists to store the data:
times = []
msg_times = []
midi_data =[]
temp_data =[]
diff_data =[]
msg_data =[]

min_note = 60
max_note = 100
# lists of dicts (initialized empty) to compare the notes of the previous time step:
previous_note_dicts = [dict()] * n_diffs

# Flush serial buffer:
serial_port.flushInput()
# Wait 100 ms:
time.sleep(0.1)

# Run over all timesteps:
for i in range(n_total_timesteps):
    timeval = datetime.datetime.now().time().isoformat(timespec='milliseconds')
    serial_string = serial_port.readline()
    # calculate live loop objects:
    temp_dicts = split_serial(serial_string)
    diff_dicts = derive_diffs(temp_dicts)
    note_on_dicts = get_note_on_dict(diff_dicts, scales, root_at_0K, slopes, channels, min_note, max_note)
    #record measurements:
    times.append(timeval)
    temp_data.append(temp_dicts)
    diff_data.append(diff_dicts)
    midi_data.append(note_on_dicts)
    # live print:
    print('T:    ',[d['T'] for d in diff_dicts])
    print('note: ', [d['note'] for d in note_on_dicts])
    msgs = []
    # midi events:
    for j in range(len(note_on_dicts)):
        # wait for a random duration of 0 to 0.2 s:
        time.sleep(random.uniform(0, 0.2))
        # only play notes if the temperature difference is not 0:
        if diff_dicts[j]['T'] != 0 and previous_note_dicts[j] != note_on_dicts[j] or i == n_total_timesteps - 1:
            if i > 0 or i == n_total_timesteps - 1:
                msg = mido.Message(**note_off_dicts[j])
                outport.send(msg)
                msgs.append(msg)
            if i < n_total_timesteps - 1:
                msg = mido.Message(**note_on_dicts[j])
                outport.send(msg)
                msgs.append(msg)
    msg_data.append(msgs)
    msg_times.append(timeval)
    previous_note_dicts = note_on_dicts
    note_off_dicts = get_note_off_dict(note_on_dicts)
```

## In case of panic interrupt the serial read by pushing the stop button above and sending:

```python
outport.panic()
```

Close the connection:

```python
serial_port.close()
```

## Appendix: Data recorded:

```python

```

**export to csv**


temperatures

```python
df = pd.DataFrame(temp_data)
df.columns = ['T' + str(l+1) for l in list(range(n_thermos))]
df['time'] = times
df.to_csv('recorded_data/temp_data_recording.csv', index=False)
```

differences

```python
df = pd.DataFrame(diff_data)
df.columns = [d['S'] for d in diff_dicts]
df['time'] = times
df.to_csv('recorded_data/diff_data_recording.csv', index=False)
```

midi 

```python
df = pd.DataFrame(midi_data)
df.columns = [d['S'] for d in diff_dicts]
df['time'] = times
df.to_csv('recorded_data/midi_data_recording.csv', index=False)
```

**create midi file**

```python
midi_port_to_file(msg_times, msg_data, 'recorded_data/live_record.mid')
```

convert midi file to wav using sf2 soundfont file at specified path:

```python
subprocess.run(["fluidsynth", 
                "/home/chief/soundfonts/KBH_Real_and_Swell_Choir.sf2", 
                "-F",
                "recorded_data/live_record.wav", 
                "recorded_data/live_record.mid"])
```

transform wav to mp3 (needs lame installed; see here: [ubuntu](https://wiki.ubuntuusers.de/LAME/), [macos](http://macappstore.org/lame/))

```python
subprocess.run(["lame", 
                "recorded_data/live_record.wav", 
                "recorded_data/live_record.mp3"])
```

```python
subprocess.run(["mscore3", 
                "recorded_data/live_record.mid",
                "-o", 
                "recorded_data/live_record.mscz"])
```
