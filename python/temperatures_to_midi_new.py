# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %%
from IPython import get_ipython

# %% [markdown]
# ## Translate temperature measurements to midi notes to synthesize with QSynth

# %%
import serial
import time
import datetime
import mido
import music21
import pandas as pd
import numpy as np
import random
import glob
import subprocess
import pathlib

# %% [markdown]
# Load functions in the file `functions.py` that has to be stored in the same directory as this notebook:

# %%
from functions import *

# %% [markdown]
# ### Setup Qsynth connection
# %% [markdown]
# Start Qsynth.
# 
# Then get the names of all output ports: 

# %%
all_outports = mido.get_output_names()
all_outports

# %% [markdown]
# The port containing fluidsynth (which is started by QSynth) is chosen:

# %%
fluidport = [s for s in all_outports if "FLUID Synth" in s]
fluidport

# %% [markdown]
# open a midi port and connect to fluidsynth:

# %%
outport = mido.open_output(fluidport[0])

# %% [markdown]
# ### Midi notes in scale
# %% [markdown]
# Choose by chord...:

# %%
chord = [0, 3, 5, 7, 10]

scale_midi_notes = get_chord_scale_notes(chord)
# to get their names:
scale_notes =  [str(music21.note.Note(i).pitch) for i in scale_midi_notes]
# Show the first 10 notes in scale:
scale_notes[:10], scale_midi_notes[:10]

# %% [markdown]
# ... alternatively choose by music21 scale object:

# %%
scale_obj = music21.scale.MinorScale('a')
scale_notes = [str(p) for p in scale_obj.getPitches('a0', 'g8')]
scale_midi_notes = [p.midi for p in scale_obj.getPitches('a0', 'g8')]
# Show the first 10 notes in scale:
scale_notes[:10],  scale_midi_notes[:10]

# %% [markdown]
# ...or choose chromatic scale:

# %%
# chromatic scale:
# scale_midi_notes = [i for i in range(128)]

# %% [markdown]
# ## Arduino 
# %% [markdown]
# Setup connection to Arduino serial line:

# %%
dev_port = glob.glob('/dev/ttyACM*')[0]


# %%
# on my ubuntu machine the serial port is located at '/dev/ttyACM1' OR '/dev/ttyACM0'
# ser = serial.Serial('/dev/ttyACM0', 9600)
serial_port = serial.Serial(dev_port, 9600)

time.sleep(0.5)

# %% [markdown]
# Read one line of the serial connection and print out the thermometer ids:

# %%
serial_string = serial_port.readline()
id_list = [i['ID'] for i in split_serial(serial_string)]
id_list

# %% [markdown]
# ## Loop parameters
# %% [markdown]
# Number of thermometers found:

# %%
n_thermos = len(id_list)
n_thermos

# %% [markdown]
# Number of possible combinations of all thermometers:

# %%
n_diffs = int((n_thermos - 1) * n_thermos / 2)
n_diffs


# %%
# The scale and the root midi note that's played at 0 K can be chosen of every pair of thermometers:
# The following pairs of thermometers exist:
pairs = list(itertools.combinations([i+1 for i in range(n_thermos)], 2))
pairs


# %%
# Choose scales for the n_diffs combinations:
scales = [scale_midi_notes] * n_diffs


# %%
# choose root notes:
# set all root midi notes to 60: 
root_at_0K = [60] * n_diffs
# or choose individually:
root_at_0K = [60, 60, 67, 67, 65, 65, 84, 84, 72, 72]
root_at_0K

# %% [markdown]
# Slopes of the linear mapping of temperature differences $T_2 - T_1$ to semi-tones $mid_2 - mid_1$:
# $$\frac{mid_2 - mid_1}{T_2 - T_1}$$

# %%
slopes = [2] * n_diffs
slopes

# %% [markdown]
# Midi channels:

# %%
channels = list(range(n_diffs))
channels

# %% [markdown]
# ## Loop
# %% [markdown]
# * reads arduino serial connection, 
# * translates the measurements to midi notes, 
# * sends the midi notes to the QSynth port, and 
# * records all the data in lists

# %%
n_total_timesteps = 10


# empty lists to store the data:
times = []
msg_times = []
midi_data =[]
temp_data =[]
diff_data =[]
msg_data =[]
# lists of dicts (initialized empty) to compare the notes of the previous time step:
previous_note_dicts = [dict()] * n_diffs

# Flush serial buffer:
serial_port.flushInput()
# Wait 100 ms:
time.sleep(0.1)

# Run over all timesteps:
for i in range(n_total_timesteps):
    timeval = datetime.datetime.now().time().isoformat(timespec='milliseconds')
    serial_string = serial_port.readline()
    # calculate live loop objects:
    temp_dicts = split_serial(serial_string)
    diff_dicts = derive_diffs(temp_dicts)
    note_on_dicts = get_note_on_dict(diff_dicts, scales, root_at_0K, slopes, channels)
    #record measurements:
    times.append(timeval)
    temp_data.append(temp_dicts)
    diff_data.append(diff_dicts)
    midi_data.append(note_on_dicts)
    # live print:
    print('T:    ',[d['T'] for d in diff_dicts])
    print('note: ', [d['note'] for d in note_on_dicts])
    msgs = []
    # midi events:
    for j in range(len(note_on_dicts)):
        if previous_note_dicts[j] != note_on_dicts[j]:
            if i > 0:
                msg = mido.Message(**note_off_dicts[j])
                outport.send(msg)
                msgs.append(msg)
            msg = mido.Message(**note_on_dicts[j])
            outport.send(msg)
            msgs.append(msg)
    msg_data.append(msgs)
    msg_times.append(timeval)
    previous_note_dicts = note_on_dicts
    note_off_dicts = get_note_off_dict(note_on_dicts)
outport.panic()

# %% [markdown]
# ## In case of panic interrupt the serial read by pushing the stop button above and sending:

# %%
outport.panic()

# %% [markdown]
# Close the connection:

# %%
serial_port.close()

# %% [markdown]
# ## Appendix: Data recorded:
# %% [markdown]
# create directory of it doesn't exist yet:

# %%
pathlib.Path("recorded_data").mkdir(parents=True, exist_ok=True)

# %% [markdown]
# **export to csv**
# %% [markdown]
# temperatures

# %%
df = pd.DataFrame(temp_data)
df.columns = ['T' + str(l+1) for l in list(range(n_thermos))]
df['time'] = times
df.to_csv('recorded_data/temp_data_recording.csv', index=False)

# %% [markdown]
# differences

# %%
df = pd.DataFrame(diff_data)
df.columns = [d['S'] for d in diff_dicts]
df['time'] = times
df.to_csv('recorded_data/diff_data_recording.csv', index=False)

# %% [markdown]
# midi 

# %%
df = pd.DataFrame(midi_data)
df.columns = [d['S'] for d in diff_dicts]
df['time'] = times
df.to_csv('recorded_data/midi_data_recording.csv', index=False)

# %% [markdown]
# **create midi file**

# %%
midi_port_to_file(msg_times, msg_data, 'recorded_data/live_record.mid')

# %% [markdown]
# convert midi file to wav using sf2 soundfont file at specified path:

# %%
subprocess.run(["fluidsynth", 
                "/home/chief/soundfonts/KBH_Real_and_Swell_Choir.sf2", 
                "-F",
                "recorded_data/live_record.wav", 
                "recorded_data/live_record.mid"])

# %% [markdown]
# transform wav to mp3 (needs lame installed; see here: [ubuntu](https://wiki.ubuntuusers.de/LAME/), [macos](http://macappstore.org/lame/))

# %%
subprocess.run(["lame", 
                "recorded_data/live_record.wav", 
                "recorded_data/live_record.mp3"])


# %%
bpm=120
ticks_per_beat=480


# %%
ticks_per_second = mido.second2tick(1, ticks_per_beat=ticks_per_beat, tempo=mido.bpm2tempo(bpm))
ticks_per_second


# %%
[[j for j in i] for i in mido.MidiFile('recorded_data/live_record.mid').tracks]


# %%
(60000 / (120 * 480))


# %%
msg_times


# %%
msg_data


# %%
tempo = mido.bpm2tempo(120)
tempo


# %%
1000000 / tempo * ticks_per_beat


# %%
get_ipython().run_line_magic('run', 'functions.py')


# %%



